import os
import sys
import xml.etree.ElementTree as ET
import shutil

class FileHeader:
	def __init__(self):
		self.path = ''
		self.filename = ''
		self.filesize = ''

	def setAttributes(self, path):
		if not os.path.isfile(path):
			sys.exit ('File ' + name + ' not found in ' + path)
		print('header making')
		self.path, self.filename = os.path.split(path)
		print ('Path: ' + self.path)
		print ('Filename: ' + self.filename)
		self.filesize = os.path.getsize(path)
		print('Filesize: ' + str(self.filesize))

	def ToString(self):
		path = os.path.join(self.path, self.filename)
		message = '<header><path>' + path + '</path>' + '<pathlength>' + str (len(path)) + '</pathlength>' + '<filesize>' + str(self.filesize) + '</filesize></header>'
		return message

	def ifSend(self):
		return True

	def getCompleteHeader(self, server):
		header = ''
		while True:
			data = server.connection.recv(50)
			if not data:
				print >>sys.stderr, 'no more data from', server.client_address
				return False
			print('Received: ' + data)
			header+=data
			print('Header: '+header)
			search = header.find('</header>')
			if search>0:
				root = ET.fromstring(header)
				for child in root:
					if child.tag == 'path':
						pathxml = child.text
						self.path, self.filename = os.path.split(pathxml)
					if child.tag == 'filesize':
						self.filesize = int(child.text)
					print child.tag, child.text
				break
		print("Header parsed")
		print(self.path + ' ' + self.filename + ' ' + str(self.filesize))
		return True

	#opens temp file
	def openFileW(self):
		name, ext = self.filename.split('.', 1)
		f = open('temp.' + ext , 'wb')
		return f

	def writeFile(self, server):
		f = self.openFileW()
		expectedData = self.filesize

		while True:
			data = server.connection.recv(50)
			if not data:
				print >>sys.stderr, 'no more data from', server.client_address
				return False

			print('Received: ' + data)
			f.write(data)
			expectedData -= len(data)
			if (expectedData == 0):
				f.close()
				#copy temp file to apropriate location
				workpath = os.path.join(self.path, self.filename)
				print('Destination file: ' + workpath)
				tempname = f.name
				print('Source temp: ' + tempname)
				if not os.path.exists(self.path):
					os.makedirs(self.path)
				shutil.move(tempname, workpath)
				break
		return True

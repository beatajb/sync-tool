import socket
import sys
import os.path
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from FileHeader import FileHeader

class Server:
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	def __init__(self, server_address):
		print >>sys.stderr, 'starting up on %s port %s' % server_address
		Server.sock.bind(server_address)
		# Listen for incoming connections
		Server.sock.listen(1)

	def exchangeData(self):

		while True:
			fh = FileHeader()
			if fh.getCompleteHeader(self):
				#send ack
				self.connection.sendall('ack')
				#process file data
				if fh.writeFile(self):
					#send ack
					self.connection.sendall('ack')
				else:
					break
			else:
				break

	def connectToClient(self):
		while True:
			# Wait for a connection
			print >>sys.stderr, 'waiting for a connection'
			self.connection, self.client_address = Server.sock.accept()
			
			try:
				print >>sys.stderr, 'connection from', self.client_address
				# Receive the data in small chunks and retransmit it
				self.exchangeData()
				
			finally:
				# Clean up the connection
				self.connection.close()

        


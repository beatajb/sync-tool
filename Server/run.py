from server import Server
from ServerArgParser import ServerArgParser
import shutil

def main():

	sap = ServerArgParser()
	workpath = sap.getArgument()
	try:
		#initialize server
		server_address = ('localhost', 50000)
		s = Server(server_address)
	
		"""blocking call"""
		s.connectToClient()
	finally:
		shutil.rmtree(workpath)
	return 0

if __name__ == '__main__':
	main()


import argparse
import os
import sys

class ServerArgParser:
	def getArgument(self):
		#argument handling
		parser = argparse.ArgumentParser()
		parser.add_argument("-df", help="Name of the empty destination directory", required=True)
		args = parser.parse_args()
		print args.df

		workpath = args.df
		#end of argument handling

		#check conditions for directory
		if os.path.isdir(workpath):
			if not os.listdir(workpath)==[]:
				sys.exit("The directory "+workpath+" is not empty.")
		else:
			try:
				os.makedirs(workpath)
			except OSError:
				sys.exit("Failed to create directory "+workpath)
		#end of directory validation block
		return workpath

import argparse
import os
import sys

class ClientArgParser:
	def getArgument(self):
		parser = argparse.ArgumentParser()
		parser.add_argument("-sf", help="Name of the directory to backup", required=True)
		args = parser.parse_args()
		print args.sf
	
		sourcef = args.sf
		#end of argument handling
		if not os.path.isdir(sourcef):
			sys.exit("The directory "+sourcef+" does not exist.")
		return sourcef

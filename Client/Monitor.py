import sys, os
import hashlib
import traceback
import time, threading

class Monitor:

	def __init__(self):

		#creates hashsum for sourcef and a dictionary of all the files in sourcef and their hashsums
		self.hashSumFiles = {}
		self.sourceDir = ''
		self.period = 10

	def setInit(self, sourcef):
		self.sourceDir = sourcef
		self.setHashSumFiles()

	def check(self):
		print(time.ctime())
		FTUTable = self.compareDir()
		print(FTUTable)
		threading.Timer(self.period, self.check).start()

	def setHashSumFiles(self):
		if not os.path.exists (self.sourceDir):
			sys.exit("Directory does not exist")

		try:
			for root, dirs, files in os.walk(self.sourceDir):
				for names in files:
					filepath = os.path.join(root,names)
					result=self.getFileHash(filepath)
					self.hashSumFiles[filepath] = result

		except:
			e = sys.exc_info()[0]
			print "error({0}):".format(e)
			traceback.print_stack()
			traceback.print_exc()
			sys.exit("Some problem")

	def getFileHash (self, filepath):
		sha1 = hashlib.sha1()
		try:
			f1 = open(filepath, 'rb')

			while 1:
				# Read file in little chunks
				buf = f1.read(4096)
				if not buf : break
				#Finished reading file
				sha1.update(buf)

			f1.close()
			print('getFileHash: ')
			print(filepath)
			print("SHA1: {0}".format(sha1.hexdigest()))
			return sha1.hexdigest()

		except:
			f1.close()
			e = sys.exc_info()[0]
			print "error({0}):".format(e)
			traceback.print_stack()
			traceback.print_exc()
			sys.exit("Can't open the file for some reason")

	def compareDir(self):
		hashSumFilesNew = {}
		if not os.path.exists (self.sourceDir):
			sys.exit("Directory does not exist")

		try:
			for root, dirs, files in os.walk(self.sourceDir):
				for names in files:
					filepath = os.path.join(root,names)
					result=self.getFileHash(filepath)
					hashSumFilesNew[filepath] = result
		except:
			e = sys.exc_info()[0]
			print "error({0}):".format(e)
			traceback.print_stack()
			traceback.print_exc()
			sys.exit("Some problem")

		FTUTable = self.compareHashTable(**hashSumFilesNew)
		return FTUTable

	def compareHashTable(self, **hashTable):
		FTUTable = {}

		for key in self.hashSumFiles:
			try:
				if self.hashSumFiles[key] == hashTable[key]:
					FTUTable[key] = False
				else:
					FTUTable[key] = True
			except:
				print('file deleted or renamed: '+key)

		return FTUTable



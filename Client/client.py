import socket
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from FileHeader import FileHeader

class Client:
	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_address = ('localhost', 50000)

	def __init__(self):
		print >>sys.stderr, 'connecting to %s port %s' % Client.server_address
		Client.sock.connect(Client.server_address)

	def walkDir (self, sourcef):
		#search sourcef for files
		if not os.listdir(sourcef)==[]:
			for dirName, subdirList, fileList in os.walk(sourcef):
				for fname in fileList:
					path = os.path.join(dirName, fname)
					print ('Processing: ' + path)
					self.exchangeData(path)
		else:
			print('Source directory %s is empty' % sourcef)

	#mysend sends a message to the server in format length##message
	def mysend(self, msg):
		totalsent = 0
		totalLen = len(msg)
		while totalsent < totalLen:
			sent = self.sock.send(msg[totalsent:])
			if sent == 0:
				raise RuntimeError("socket connection broken")
			totalsent = totalsent + sent

	def getACK(self):
		ack = Client.sock.recv(16)
		if not ack == 'ack':
			print ('Error while sending')
			return False
		else:
			return True

	def exchangeData(self, path):
		
		# Send header
		fh = FileHeader()
		fh.setAttributes(path)
		if fh.ifSend:
			#Start sending to server
			#Send header
			msg = fh.ToString()
			received = False
			while not received: 
				self.mysend(msg)
				received = self.getACK()

			#send file data
			received = False
			
			while not received:
				f = open(path, 'rb')
				l = f.read(1024)
				while (l):
					self.mysend(l)
					print('Sent ',repr(l))
					l = f.read(1024)
				f.close()
				received = self.getACK()

	def close(self):
		print >>sys.stderr, 'closing socket'
		Client.sock.close()
